package com.cognizant.examples.feignexampleserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeignExampleServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeignExampleServerApplication.class, args);
	}

}
